<?php 
// Permanent Redirects For Old Website
Route::redirect('news', '/pages/events', 301);
Route::redirect('links', '/', 301);
Route::redirect('history-of-the-club', '/pages/our-history', 301);

// Home Page
Route::get('/', 'HomeController@index');

// Content Styles
Route::get('/style-guide', 'HomeController@styleGuide');

// Contact Form
Route::group(['prefix' => '/contact'], function () {
    Route::get('/', 'ContactController@index');
    Route::post('save-message', 'ContactController@saveMessage');
    Route::get('success', 'ContactController@success');
	
	Route::post('save-newsletter', 'ContactController@saveNewsletter');
	Route::get('newsletter-success', 'ContactController@newslettersuccess');
});

// Pages Module
Route::group(['prefix' => '/pages'], function () {
    Route::get('{category}', 'PagesController@index');    
	Route::get('{category}/{page}', 'PagesController@index');    
});

//// News Module
//Route::group(['prefix' => '/news'], function () {
//	Route::get('', 'NewsController@list'); 	
//	
//	Route::get('archive', 'NewsController@archive'); 
//	Route::get('archive/{age}', 'NewsController@archive'); 
//	
//    Route::get('{category}', 'NewsController@list');    
//	Route::get('{category}/{page}', 'NewsController@item'); 
//	
//});

// Faqs Module
Route::group(['prefix' => '/faqs'], function () {
	Route::get('', 'FaqsController@index'); 
    Route::get('{category}', 'FaqsController@index');    	
});

// Documents Module
Route::group(['prefix' => '/documents'], function () {
	Route::get('', 'DocumentsController@index'); 
    Route::get('{category}', 'DocumentsController@index');    	
});

// Gallery Module
Route::group(['prefix' => '/gallery'], function () {
	Route::get('', 'GalleryController@index'); 
    Route::get('{category}', 'GalleryController@detail');    	
});

// Projects Module
Route::group(['prefix' => '/projects'], function () {
	Route::get('', 'ProjectsController@list'); 			
	
    Route::get('{category}', 'ProjectsController@list');    
	Route::get('{category}/{page}', 'ProjectsController@item'); 
	
});

// Testimonials Module
Route::group(['prefix' => '/testimonials'], function () {
	Route::get('', 'TestimonialsController@index');     
});

// Team Module
Route::group(['prefix' => '/team'], function () {
    Route::get('', 'TeamController@index');
    Route::get('{category}', 'TeamController@index');
    Route::get('{category}/{member}', 'TeamController@member');
});