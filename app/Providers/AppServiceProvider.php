<?php

namespace App\Providers;

use App\Member;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Module;
use App\Setting;
use App\User;
use App\PageCategory;
use Carbon\Carbon;
use App\Helpers\General;
use App\ImagesHomeSlider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ////ADMIN
        if (!session()->has('pagination-count')) {
            session(['pagination-count' => 25]);
        }

        if (!session()->has('sidebar-state')) {
            session(['sidebar-state' => '']);
        }

        Validator::extend('date_checker', function ($attribute, $value, $parameters, $validator) {
            $archive_date = Carbon::createFromFormat('d/m/Y', $value);
            $start_date = Carbon::createFromFormat('d/m/Y', $parameters[0]);

            return $archive_date->gt($start_date);
        });

        Validator::extend('unique_store', function ($attribute, $value, $parameters, $validator) {
            $table_name = $parameters[0];
            if (\DB::table($table_name)->where($attribute,'=',$value)->where('is_deleted', '=', 'false')->exists()) {
                return false;
            }
            return true;
        });

        Validator::extend('unique_update', function ($attribute, $value, $parameters, $validator) {
            $table_name = $parameters[0];
            $id = $parameters[1];
            if (\DB::table($table_name)->where($attribute,'=',$value)->where('id', '!=', $id)->where('is_deleted', '=', 'false')->exists()) {
                return false;
            }
            return true;
        });

        Validator::extend('gallery_image_checker', function ($attribute, $value, $parameters, $validator) {
            if($value>0){
                return true;
            }
            return false;
        });

        view()->composer('admin/partials/header', function ($view) {

            $settings = Setting::where('key', '=', 'company-name')->first();
            $view->with('company_name', $settings->value);
        });

        view()->composer('admin/partials/menu', function ($view) {

            $pages = Module::where('slug', '=', 'pages')->first();
            $view->with('pages_status', $pages->status);

            $news = Module::where('slug', '=', 'news')->first();
            $view->with('news_status', $news->status);

            $gallery = Module::where('slug', '=', 'gallery')->first();
            $view->with('gallery_status', $gallery->status);

            $contact = Module::where('slug', '=', 'contact')->first();
            $view->with('contact_status', $contact->status);

            $faqs = Module::where('slug', '=', 'faqs')->first();
            $view->with('faqs_status', $faqs->status);

            $members = Module::where('slug', '=', 'members')->first();
            $view->with('members_status', $members->status);

            $documents = Module::where('slug', '=', 'documents')->first();
            $view->with('documents_status', $documents->status);
			
			$projects = Module::where('slug', '=', 'projects')->first();
            $view->with('projects_status', $projects->status);
			
			$testimonials = Module::where('slug', '=', 'testimonials')->first();
            $view->with('testimonials_status', $testimonials->status);

            $team = Module::where('slug', '=', 'team')->first();
            $view->with('team_status', $team->status);
        });

        ////SITE
        ///
        // Home Page
		view()->composer('site/index', function ($view) {

			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $view->with('company_name', $settings->value);
						
			// Intro Text
			$settings = Setting::where('key', '=', 'home-intro-text')->first();
            $view->with('home_intro_text', $settings->value);	
			
			// Page - About Us
			$general = new General();
			$page_about = $general->getHomePage(4);
			$view->with('page_about', $page_about);			
			
			// Page - Events
			$general = new General();
			$page_events = $general->getHomePage(3);
			$view->with('page_events', $page_events);							
			
			// News Panel
			$general = new General();
			$home_news = $general->getHomeNews(true);
			$view->with('home_news', $home_news);	
			
			// Project Panel
			$general = new General();
			$home_projects = $general->getHomeProjects(true);
			$view->with('home_projects', $home_projects);	
			
			// Testimonials Panel
			$general = new General();
			$home_testimonials = $general->getHomeTestimonials(true);
			$view->with('home_testimonials', $home_testimonials);	
			
			// Awards Panel
			$general = new General();
			$home_awards = $general->getHomeAwards(true);
			$view->with('home_awards', $home_awards);	
        });
		
		view()->composer('site/layouts/app', function ($view) {

			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $view->with('company_name', $settings->value);
			
			// Meta Title
			$settings = Setting::where('key', '=', 'meta-title')->first();
            $view->with('meta_title', $settings->value);
			
			// Meta Keywords
			$settings = Setting::where('key', '=', 'meta-keywords')->first();
            $view->with('meta_keywords', $settings->value);
			
			// Meta Description
			$settings = Setting::where('key', '=', 'meta-description')->first();
            $view->with('meta_description', $settings->value);
			
			// Intro Text
			$settings = Setting::where('key', '=', 'home-intro-text')->first();
            $view->with('home_intro_text', $settings->value);
			
			// Go Live Date
			$settings = Setting::where('key', '=', 'live-date')->first();
            $view->with('live_date', $settings->value);
			
			// Google Analytics
			$settings = Setting::where('key', '=', 'google-analytics')->first();
            $view->with('google_analytics', $settings->value);
        });
		
        //Populate Main Menu
        view()->composer('site/partials/navigation', function ($view) {
            // Navigation
			$general = new General();
			$navigation = $general->getNavigation(true);
			
			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $company_name = $settings->value;

			// Phone Number
            $settings = Setting::where('key', '=', 'phone-number')->first();
            $phone_number = $settings->value;
			
            $view->with(array(				
                'navigation' => $navigation,
				'company_name' => $company_name,
				'phone_number' => $phone_number,
            ));
        });
		
		//Populate Slider Carousel
        view()->composer('site/partials/carousel', function ($view) {            
			// Slider Images
            $images = ImagesHomeSlider::where('status', '=', 'active')->orderBy('position', 'desc')->get();            
			
            $view->with(array(
                'images' => $images,				
            ));
        });
		
		//Populate Slider Carousel - Mobile
        view()->composer('site/partials/carousel-mobile', function ($view) {            
			// Slider Images
            $images = ImagesHomeSlider::where('status', '=', 'active')->orderBy('position', 'desc')->get();            
			
            $view->with(array(
                'images' => $images,				
            ));
        });
		
		//Populate Slider Carousel Inner
        view()->composer('site/partials/carousel-inner', function ($view) {
			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $company_name = $settings->value;
			
			// Slider Images
            $images = ImagesHomeSlider::where('status', '=', 'active')->orderBy('position', 'desc')->get();            
			
            $view->with(array(
				'company_name' => $company_name,
                'images' => $images,				
            ));
        });
		
		//Populate Footer
        view()->composer('site/partials/footer', function ($view) {
            // Navigation
			$general = new General();
			$modules = $general->getNavigation();
			
			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $company_name = $settings->value;

			// Contact Details
            $settings = Setting::where('key', '=', 'contact-details')->first();
            $contact_details = $settings->value;
			
			// Phone Number
            $settings = Setting::where('key', '=', 'phone-number')->first();
            $phone_number = $settings->value;
			
			// Contact Email
            $settings = Setting::where('key', '=', 'contact-email')->first();
            $contact_email = $settings->value;
			
			// Social - Facebook
            $settings = Setting::where('key', '=', 'social-facebook')->first();
            $social_facebook = $settings->value;
			
			// Social - Twitter
            $settings = Setting::where('key', '=', 'social-twitter')->first();
            $social_twitter = $settings->value;
			
			// Social - LinkedIn
            $settings = Setting::where('key', '=', 'social-linkedin')->first();
            $social_linkedin = $settings->value;
			
			// Social - GooglePlus
            $settings = Setting::where('key', '=', 'social-googleplus')->first();
            $social_googleplus = $settings->value;
			
			// Social - Instagram
            $settings = Setting::where('key', '=', 'social-instagram')->first();
            $social_instagram = $settings->value;
			
			// Social - Pinterest
            $settings = Setting::where('key', '=', 'social-pinterest')->first();
            $social_pinterest = $settings->value;
			
			// Social - Youtube
            $settings = Setting::where('key', '=', 'social-youtube')->first();
            $social_youtube = $settings->value;

			
            $view->with(array(
                'modules' => $modules,
				'company_name' => $company_name,
				'contact_details' => $contact_details,
				'phone_number' => $phone_number,
				'contact_email' => $contact_email,
				'social_facebook' => $social_facebook,
				'social_twitter' => $social_twitter,
				'social_linkedin' => $social_linkedin,
				'social_googleplus' => $social_googleplus,
				'social_instagram' => $social_instagram,
				'social_pinterest' => $social_pinterest,
				'social_youtube' => $social_youtube,
            ));
        });

        //contact form validation
        Validator::extend(
            'recaptcha',
            'App\\Validators\\ReCaptcha@validate'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
