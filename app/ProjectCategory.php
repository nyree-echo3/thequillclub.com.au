<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class ProjectCategory extends Model
{
    use SortableTrait;

    protected $table = 'project_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'category_id');
    }
}
