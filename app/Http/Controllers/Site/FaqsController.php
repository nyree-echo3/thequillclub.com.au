<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Faq;
use App\FaqCategory;

class FaqsController extends Controller
{
    public function index($category_slug = "", $mode = ""){
		$side_nav = $this->getCategories();
		
		if (sizeof($side_nav) > 0)  {
			if ($category_slug == "")  {		   		   
			   $items = $this->getItems($side_nav[0]->id, $mode);		
			} else {
			  $category = $this->getCategory($category_slug);
			  $items = $this->getItems($category[0]->id, $mode);		
			}		
		}
		
		return view('site/faqs/list', array(         
			'side_nav' => $side_nav,				
			'items' => (sizeof($side_nav) > 0 ? $items : null),	
			'mode' => $mode,
        ));

    }
	
	public function getCategories(){
		$categories = FaqCategory::whereHas("faqs")->where('status', '=', 'active')->get();		
		return($categories);
	}
	
	public function getCategory($category_slug){
		$categories = FaqCategory::where('slug', '=', $category_slug)->get();		
		return($categories);
	}
	
	public function getItems($category_id, $mode){
		if ($mode == "preview") {
		   $items = Faq::where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		} else {
		   $items = Faq::where('status', '=', 'active')->where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		}
		return($items);
	}		
}
