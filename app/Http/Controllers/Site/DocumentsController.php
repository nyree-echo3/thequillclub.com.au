<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Document;
use App\DocumentCategory;

class DocumentsController extends Controller
{
    public function index($category_slug, $page_slug = "", $mode = ""){

		$side_nav = $this->getCategories();
	
		if (sizeof($side_nav) > 0)  {
		   if ($category_slug == "")  {		   		   
		   $items = $this->getItems($side_nav[0]->id, $mode);		
		   } else {
    	     $category = $this->getCategory($category_slug);
		     $items = $this->getItems($category[0]->id, $mode);		
		   }		
		}
		
		return view('site/documents/list', array(         
			'side_nav' => $side_nav,
			'category' => (sizeof($side_nav) > 0 ? $side_nav[0] : null),			
			'items' => (sizeof($side_nav) > 0 ? $items : null),		
			'mode' => $mode,
        ));
    }
	
	public function getCategories(){
		$categories = DocumentCategory::whereHas("documents")->where('status', '=', 'active')->get();		
		return($categories);
	}
	
	public function getCategory($category_slug){
		$categories = DocumentCategory::where('slug', '=', $category_slug)->get();		
		return($categories);
	}
	
	public function getItems($category_id, $mode){
		if ($mode == "preview") {
		   $items = Document::where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		} else {
		   $items = Document::where('status', '=', 'active')->where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		}
		return($items);
	}			
}
