<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Module;
use Illuminate\Http\Request;
use App\TeamMember;
use App\TeamCategory;
use App\Http\Controllers\Site\PagesController;

class TeamController extends Controller
{
    public function index(Request $request)
    {
        $module = Module::where('slug', '=', "team")->first();
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        if($request->category==''){
            $selected_category = TeamCategory::where('status','=','active')->whereHas('members')->orderBy('position', 'desc')->first();
        }else{
            $selected_category = TeamCategory::where('slug','=',$request->category)->first();
        }

        $items = null;
        $meta_title_inner = "Team";
        $meta_keywords_inner = "Team";
        $meta_description_inner = "Team";

        if($selected_category){
            $items = TeamMember::where('category_id','=', $selected_category->id)->orderBy('position', 'desc')->get();

            $meta_title_inner = $selected_category->name. " - Team";
            $meta_description_inner = $selected_category->name . " - Team";
        }

        $mode="";

        $category = (new PagesController)->getCategory('about');
        $side_nav = (new PagesController)->getPages($category[0]->id);
        $page = (new PagesController)->getPage($category[0]->id, 'our-team', $mode);

        return view('site/team/list', array(
            'categories' => $categories,
            'items' => $items,
            'module' => $module,
            'meta_title_inner' => $meta_title_inner,
            'meta_keywords_inner' => $meta_keywords_inner,
            'meta_description_inner' => $meta_description_inner,
            'page_type' => "Pages",
            'side_nav' => $side_nav,
            'category' => $category,
            'page' => $page,
            'mode' => $mode,
        ));

    }

    public function member($category, $team_member)
    {
        $module = Module::where('slug', '=', "team")->first();
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();
        $member = TeamMember::where('slug','=',$team_member)->first();

        $mode="";

        $category = (new PagesController)->getCategory('about');
        $side_nav = (new PagesController)->getPages($category[0]->id);
        $page = (new PagesController)->getPage($category[0]->id, 'our-team', $mode);

        return view('site/team/item', array(
            'categories' => $categories,
            'team_member' => $member,
            'module' => $module,
            'meta_title_inner' => $member->name.'- Team',
            'meta_keywords_inner' => $member->name,
            'meta_description_inner' => $member->name,
            'page_type' => "Pages",
            'side_nav' => $side_nav,
            'category' => $category,
            'page' => $page,
            'mode' => $mode,
        ));
    }
}
