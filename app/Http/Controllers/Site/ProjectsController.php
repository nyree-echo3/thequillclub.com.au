<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Project;
use App\ProjectCategory;
use App\Module;
use App\Page;

class ProjectsController extends Controller
{
    public function list(Request $request, $category_slug = "", $item_slug = ""){   
		$module = Module::where('slug', '=', "projects")->first();
		
		if ($category_slug != "awards") {
			// Projects
			$side_nav = $this->getCategories();	

			if (sizeof($side_nav) > 0)  {
				if ($category_slug == "")  {
				   // Get Latest Projects
				   $category_name = "Latest Projects";	

				   $items = $this->getProjects();
				} elseif ($category_slug != "" && $item_slug == "") {
				  // Get Category Projects	
				  $category = $this->getCategory($category_slug);
				  $category_name = $category->name;	

				  $items = $this->getProjects($category->id);			  
				} 		
			}
			
			return view('site/projects/list', array( 
				'module' => $module,
				'side_nav' => $side_nav,
				'category_name' => (sizeof($side_nav) > 0 ? $category_name : null),
				'items' => (sizeof($side_nav) > 0 ? $items : null),			
			));
			
		} else  {
			// Pages
			$pages = new PagesController();			    	

			$category = $pages->getCategory("about");

			$side_nav = $pages->getPages($category[0]->id);
			$page = $pages->getPages($category[0]->id)->first();
			$page->slug = "awards";
			
			// Projects - Awards			
		    $items = $this->getProjects(4);	
			
			return view('site/projects/list', array(
				'page_type' => "Pages",
				'side_nav' => $side_nav,
				'category' => $category,
				'category_name' => "Awards",
				'page' => $page,
				'mode' => "",
				'items' => $items,	
			));		
		}				
    }
	
    public function item(Request $request, $category_slug, $item_slug){
		$module = Module::where('slug', '=', "projects")->first();
		
		if ($category_slug != "awards") {
			// Projects
			$side_nav = $this->getCategories();				  			
			$project_item = $this->getProjectItem($item_slug);			  

			return view('site/projects/item', array(  
				'module' => $module,
				'side_nav' => $side_nav,					
				'project_item' => $project_item,	
				'category_name' => "",
			));
		} else  {
			// Pages
			$pages = new PagesController();			    	

			$category = $pages->getCategory("about");

			$side_nav = $pages->getPages($category[0]->id);
			$page = $pages->getPages($category[0]->id)->first();
			$page->slug = "awards";
			
			// Projects - Awards			
		    $project_item = $this->getProjectItem($item_slug);	
			
			return view('site/projects/item', array(
				'page_type' => "Pages",
				'side_nav' => $side_nav,
				'category' => $category,
				'category_name' => "Awards",
				'page' => $page,
				'mode' => "",
				'project_item' => $project_item,			
			));		
			
		}
		
    }		
	public function getCategories(){
		$categories = ProjectCategory::where('status', '=', 'active')->get();		
		return($categories);
	}	
	
	public function getProjects($category_id = "", $limit = 2){
		if ($category_id == "")  {
			$projects = Project::where('status', '=', 'active')	
				        ->where('category_id', '<>', 4)
				        ->with("images")
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
		   $projects = Project::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
			            ->with("images")
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($projects);
	}	
	
	public function getFeaturedProjects($category_id = "", $limit = 2){
		if ($category_id == "")  {
			$projects = Project::where(['status' => 'active', 'feature' => 'active'] )	
				        ->with("images")
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
		   $projects = Project::where(['status' => 'active', 'feature' => 'active'])
			            ->where('category_id', '=', $category_id)						
			            ->with("images")
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($projects);
	}
	  
	public function getProjectItem($item_slug){		
		$project = Project::where(['status' => 'active', 'slug' => $item_slug])
			        ->where('status', '=', 'active')
			        ->first();						
		return($project);
	}
	
	public function getCategory($category_slug){
		$categories = ProjectCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
}
