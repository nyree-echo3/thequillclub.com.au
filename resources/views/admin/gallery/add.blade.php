@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Gallery</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/gallery') }}"><i class="fas fa-images"></i> Gallery</a></li>
                <li class="active">Add New Image</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Image</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/gallery/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Category *</label>

                                    <div class="col-sm-10{{ ($errors->has('category_id')) ? ' has-error' : '' }}">
                                        @if(count($categories)>0)
                                            <select name="category_id" class="form-control select2"
                                                    data-placeholder="All" style="width: 100%;">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}"{{ (old('category_id') == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <div class="callout callout-danger">
                                                <h4>No category found!</h4>
                                                <a href="{{ url('dreamcms/gallery/add-category') }}">Please click here
                                                    to add category</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div id="images-container" class="form-group {{ ($errors->has('imageCount')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Images</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="imageCount" name="imageCount" value="{{ (old('imageCount') != "" ? old('imageCount') : 0) }}">

                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload Images</button>
                                        @php
                                            $class = ' invisible';
                                            if(old('imageCount') && old('imageCount') > 0){
                                                $class = '';
                                            }
                                            $i=0;
                                        @endphp
                                        <br/><br/>
                                        <span id="added_image">
                                        @foreach($images as $image)
                                                <div id="imgBox{{ $i }}" class="image-box sortable">
                                                <input id="imgPosition{{ $i }}" name="imgPosition{{ $i }}" class="imgPosition" type="hidden" value="{{ $image->position }}">
                                                <input id="imgDelete{{ $i }}" name="imgDelete{{ $i }}" type="hidden" value="{{ $image->delete }}">
                                                <input id="imgType{{ $i }}" name="imgType{{ $i }}" type="hidden" value="new">

                                                <div class="image-box-toolbox">

                                                    <a class="tool" title="Delete" href="#" onclick="image_delete({{ $i }}); return false;"><i class="fa fa-trash-alt"></i></a>

                                                    <div class="pull-right" style="margin-right: 10px">
                                                        <div style="width: 33px; height: 22px;">
                                                            @php
                                                                $status = 'active';
                                                                if(old('imgStatus' . $i)=='on'){
                                                                    $status = 'active';
                                                                }else{
                                                                    $status = '';
                                                                }
                                                            @endphp
                                                            <input id="imgStatus{{ $i }}" name="imgStatus{{ $i }}" data-id="{{ $i }}" type="checkbox" class="image_status" data-size="mini" {{ ($image->status == 'active' || $image->status == 'on') ? ' checked' : null }}>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="image-box-img">
                                                    <image class="image-item" src="{{ $image->location }}"/>
                                                    <input id="imgLocation{{ $i }}" name="imgLocation{{ $i }}" type="hidden" value="{{ $image->location }}">
                                                </div>

                                            </div>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        </span>
                                        @if ($errors->has('imageCount'))
                                            <small id="images-help-block" class="help-block">{{ $errors->first('imageCount') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="description"
                                                  placeholder="Description">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                    $status = 'active';
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status *</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini"
                                                   name="live" {{ $status == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ url('dreamcms/gallery') }}" class="btn btn-info pull-right"
                                       data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                       data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                       data-btn-cancel-label="No">Cancel</a>
                                    <button type="submit" class="btn btn-info pull-right" name="action"
                                            value="save_close">Save & Close
                                    </button>
                                    <button type="submit" class="btn btn-info pull-right" name="action" value="save">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $("#image-popup").click(function () {
                openPopup();
            });

            for (i = 0; i < $('#imageCount').val(); i++) {
                $('#imgStatus' + i).bootstrapToggle();
            }
        });

        function openPopup() {
            imageCounter = $('#imageCount').val();

            CKFinder.popup({
                chooseFiles: true,
                width: 800,
                height: 600,

                onInit: function (finder) {

                    finder.on('files:choose', function (evt) {
                        var files = evt.data.files;

                        files.forEach(function (file, i) {
                            folder = file.get('folder');

                            strHtml = '<div id="imgBox' + imageCounter + '" class="image-box sortable">';

                            strHtml += '<input id="imgPosition' + imageCounter + '" name="imgPosition' + imageCounter + '" type="hidden" class="imgPosition" value="' + imageCounter + '">';
                            strHtml += '<input id="imgDelete' + imageCounter + '" name="imgDelete' + imageCounter + '" type="hidden" value="">';
                            strHtml += '<input id="imgType' + imageCounter + '" name="imgType' + imageCounter + '" type="hidden" value="new">';
                            strHtml += '<div class="image-box-toolbox">';

                            strHtml += '<a class="tool" title="Delete" href="#" onclick="image_delete(' + imageCounter + '); return false;">';
                            strHtml += '<i class="fa fa-trash-alt"></i>';
                            strHtml += '</a>';

                            strHtml += '<div class="pull-right" style="margin-right: 10px">';
                            strHtml += '<div style="width: 33px; height: 22px;">';
                            strHtml += '<input id="imgStatus' + imageCounter + '" name="imgStatus' + imageCounter + '" data-id="' + imageCounter + '" type="checkbox" class="image_status" data-size="mini" checked>';
                            strHtml += '</div>';
                            strHtml += '</div>';

                            strHtml += '</div>';

                            strHtml += '<div class="image-box-img">';
                            strHtml += '<image class="image-item" src="' + folder.getUrl() + file.get('name') + '">';
                            strHtml += '<input id="imgLocation' + imageCounter + '" name="imgLocation' + imageCounter + '" type="hidden" value="' + folder.getUrl() + file.get('name') + '">';

                            strHtml += '</div>';

                            strHtml += '</div>';

                            $('#added_image').append(strHtml);
                            $('#imgStatus' + imageCounter).bootstrapToggle();

                            imageCounter++;

                            $('#imageCount').val(imageCounter);

                        });
                        $('#images-container').removeClass('has-error');
                        $('#images-help-block').hide();
                    });
                }
            });
        }

        function image_delete(id) {
            $('#imgBox' + id).remove();
            $('#imageCount').val($('#imageCount').val() - 1);
        }
    </script>
@endsection