@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Navigation</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li class="active">Navigation</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">
                    @if(count($navigation))
                        <table class="table table-hover">
                            <tr>
                                <th></th>
                                <th>Top Menu Item</th>
                                <th>Module</th>
                                <th>Status</th>
                            </tr>
                            <tbody class="sortable" data-entityname="modules">
                            @foreach($navigation as $item)
                                <tr id='item_{{ $item["name"] == "Pages" ? 'P::' : 'M::' }}{{ $item["id"] }}' data-itemId="{{ $item["id"] }}">
                                    <td class="sortable-handle"><span class="glyphicon glyphicon-sort"></span></td>
                                    <td>{{ $item["display_name"] }}</td>
                                    <td>{{ $item["name"] }}</td>
                                    <td>
                                        @if ($item["name"] != "Pages")
                                            <input id="module_tm_{{ $item["id"] }}" data-id="{{ $item["id"] }}" class="top_menu_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $item["top_menu"] == 'active' ? ' checked' : null }}>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        No record
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        var changePosition = function(requestData){
            $.ajax({
                'url': '{{ url('dreamcms/sort') }}',
                'type': 'POST',
                'data': requestData,
                'success': function(data) {
                    if (data.success) {
                        toastr.options = {"closeButton": true}
                        toastr.success('Saved');
                    } else {
                        toastr.options = {"closeButton": true}
                        toastr.error(data.errors);
                    }
                },
                'error': function(){
                    toastr.options = {"closeButton": true}
                    toastr.error('Something wrong');
                }
            });
        };

        $(document).ready(function(){
            var $sortableTable = $('.sortable');
            if ($sortableTable.length > 0) {
                $sortableTable.sortable({
                    handle: '.sortable-handle',
                    axis: 'y',
                    update: function(a, b){

                        var entityName = $(this).data('entityname');
                        var $sorted = b.item;

                        var $previous = $sorted.prev();
                        var $next = $sorted.next();

                        if ($previous.length > 0) {
                            changePosition({
                                parentId: $sorted.data('parentid'),
                                type: 'moveBefore',
                                entityName: entityName,
                                id: $sorted.data('itemid'),
                                positionEntityId: $previous.data('itemid')
                            });
                        } else if ($next.length > 0) {
                            changePosition({
                                parentId: $sorted.data('parentid'),
                                type: 'moveAfter',
                                entityName: entityName,
                                id: $sorted.data('itemid'),
                                positionEntityId: $next.data('itemid')
                            });
                        } else {
                            console.error('Something wrong!');
                        }
                    },
                    cursor: "move"
                });
            }

            $('.top_menu_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "/dreamcms/settings/"+$(this).data('id')+"/change-tm-status",
                    data:  {
                        'tm_status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    },
                    'error': function(response, error){
                        toastr.options = {"closeButton": true}
                        toastr.error('Something wrong');
                    }
                });
            });

        });
    </script>
@endsection