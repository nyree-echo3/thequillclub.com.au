@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Settings</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li class="active">Contacts</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Contact Details</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/update-contacts') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">                                
								<div class="form-group {{ ($errors->has('email')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Contact Email</label>

									<div class="col-sm-10">
										<input type="text" class="form-control" name="email" placeholder="Contact Email" value="{{ $email->value }}">
										@if ($errors->has('email'))
											<small class="help-block">{{ $errors->first('email') }}</small>
										@endif
									</div>
								</div>
                                
                                <div class="form-group {{ ($errors->has('emailtext')) ? ' has-error' : '' }}">   
                                    <label class="col-sm-2">Contact Email - Text</label>
                                </div>
                                <div class="form-group {{ ($errors->has('emailtext')) ? ' has-error' : '' }}">   
                                    <div class="col-sm-12">
                                        <textarea id="emailtext" name="emailtext" rows="20" cols="80"
                                                  style="height: 750px;">{{ $emailtext->value }}</textarea>
                                        @if ($errors->has('emailtext'))
                                            <small class="help-block">{{ $errors->first('emailtext') }}</small>
                                        @endif
                                    </div>
                                </div>
                                                          
                               <hr>
                               
                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">   
                                    <label class="col-sm-2">Contact Details</label>
                                </div>
                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">   
                                    <div class="col-sm-12">
                                        <textarea id="details" name="details" rows="20" cols="80"
                                                  style="height: 750px;">{{ $details->value }}</textarea>
                                        @if ($errors->has('details'))
                                            <small class="help-block">{{ $errors->first('details') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <hr>
                            
                            <div class="form-group {{ ($errors->has('emailformpagetext')) ? ' has-error' : '' }}">   
                                    <label class="col-sm-2">Contact Page - Text</label>
                                </div>
                                <div class="form-group {{ ($errors->has('emailformpagetext')) ? ' has-error' : '' }}">   
                                    <div class="col-sm-12">
                                        <textarea id="emailformpagetext" name="emailformpagetext" rows="20" cols="80"
                                                  style="height: 750px;">{{ $emailformpagetext->value }}</textarea>
                                        @if ($errors->has('emailformpagetext'))
                                            <small class="help-block">{{ $errors->first('emailformpagetext') }}</small>
                                        @endif
                                    </div>
                                </div>                                                                                         

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {            
			CKEDITOR.replace('emailtext');		
			CKEDITOR.replace('details');
			CKEDITOR.replace('emailformpagetext');			
        });
    </script>
@endsection