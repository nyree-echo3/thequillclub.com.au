@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Users</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-user"></i> Users</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">User List</h3>

                    @can('add-user')
                    <div class="pull-right box-tools">
                        <a href="{{ url('dreamcms/user/add') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    @endcan
                </div>
                <div class="box-body">
                    @if(count($users))
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th class="pull-right">Actions</th>
                            </tr>
                            @foreach($users as $user)
                                @php
                                  if($user->role=='admin'){
                                    $style = ' style="color: #dd4b39"';
                                  }else{
                                    $style = '';
                                  }
                                @endphp
                                <tr>
                                    <td{!! $style !!}>{{ $user->name }}</td>
                                    <td{!! $style !!}>{{ $user->email }}</td>
                                    <td>
                                        <input id="user_{{ $user->id }}" data-id="{{ $user->id }}" class="user_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $user->status == 'active' ? ' checked' : null }}>
                                    </td>

                                    <td>
                                        <div class="pull-right">
                                            @can('edit-user')
                                            <a href="{{ url('dreamcms/user/'.$user->id.'/edit') }}" class="tool" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                                            @endcan
                                            @can('delete-user')
                                            <a href="{{ url('dreamcms/user/'.$user->id.'/delete') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                            @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection

@section('inline-scripts')
<script type="text/javascript">
    $(document).ready(function () {

        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]'
        });

        $('.user_status').change(function() {
            $.ajax({
                type: "POST",
                url: "user/"+$(this).data('id')+"/change-user-status",
                data:  {
                    'status':$(this).prop('checked')
                },
                success: function (response) {
                    if(response.status=="success"){
                        toastr.options = {"closeButton": true}
                        toastr.success('Status has been changed');
                    }
                }
            });
        });
    });
</script>
@endsection