@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Profile</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Profile</a></li>
            </ol>
        </section>

        <section class="content">

            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Change Information</h3>
                    </div>
                    <form method="post" class="form-horizontal" action="{{ url('dreamcms/profile/save-information') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name"
                                           value="{{ old('name',$user->name) }}">
                                    @if ($errors->has('name'))
                                        <small class="help-block">{{ $errors->first('name') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ ($errors->has('email')) ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="email"
                                           value="{{ old('email',$user->email) }}">
                                    @if ($errors->has('email'))
                                        <small class="help-block">{{ $errors->first('email') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Change Password</h3>
                    </div>

                    <form method="post" class="form-horizontal" action="{{ url('dreamcms/profile/change-password') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group {{ ($errors->has('current_password')) ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="current_password">
                                    @if ($errors->has('current_password'))
                                        <small class="help-block">{{ $errors->first('current_password') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ ($errors->has('password')) ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">New Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <small class="help-block">{{ $errors->first('password') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ ($errors->has('password_confirmation')) ? ' has-error' : '' }}">
                                <label class="col-sm-3 control-label">New Password Confirm</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password_confirmation">
                                    @if ($errors->has('password_confirmation'))
                                        <small class="help-block">{{ $errors->first('password_confirmation') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    </div>
@endsection