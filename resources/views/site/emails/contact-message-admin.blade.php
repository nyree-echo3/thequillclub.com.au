<!DOCTYPE html>
<html>
<body>
<table class="table">
    @foreach(json_decode($contact_message->data) as $field)
        <tr>
            <th style="width:20%">{{ $field->field }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
