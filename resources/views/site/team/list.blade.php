@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-pages')

                <div class="col-sm-9 blog-main">

                    <div class="blog-post row">
                        <h1 class="blog-post-title"></h1>
                        @if($items)

                            @foreach($items as $item)
                                <a href="{{ url('') }}/team/{{ $item->category->slug }}/{{$item->slug}}">
                                    <div class="col-xs-1 col-sm-6 col-lg-4 team-item">
                                        <div class="team-item-img" style="background: url({{ $item->photo }}); "></div>
                                        <div class="team-name-band">
                                            <a href="{{ url('') }}/team/{{ $item->category->slug }}/{{$item->slug}}">{{ $item->name }}<br><span class="team-name-band-title">{{ $item->job_title }}</span></a>
                                        </div>
                                    </div>
                                </a>
                            @endforeach

                        @else
                            <p>Currently there is no team member to display.</p>
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
