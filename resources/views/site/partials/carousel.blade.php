<div class="panelNav">
 <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
 
  <ol class="carousel-indicators">
    <?php $counter = 0; ?>
    @foreach($images as $image)        
       <li data-target="#myCarousel" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>       
       <?php $counter++;  ?>
    @endforeach 
  </ol>
  
  <div class="carousel-inner">  
         
        <?php $counter = 0; ?>
        @foreach($images as $image) 
            <?php $counter++;  ?>
               
			<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
			  <img class="slide" src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}">			  			  
			    
			    <div class="carousel-caption">
					
				</div><!-- /.carousel-caption -->  
				
				
		
			</div><!-- /.carousel-item -->  
        @endforeach
   
  </div><!-- /.carousel-inner -->    
 
      
	  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	  </a>
  </div><!-- /#myCarousel -->
</div><!-- /.panelNav --> 
  
@section('inline-scripts')  
   <script type="text/javascript"> 	   
		$('#myCarousel').carousel({
		  interval: 15000 // 15 seconds
		})				
</script>			
@endsection