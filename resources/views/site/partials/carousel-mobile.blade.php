 <div id="myCarousel-mobile" class="carousel slide carousel-fade" data-ride="carousel">  
  <div class="carousel-inner">
    
        <?php $counter = 0; ?>
        @foreach($images as $image) 
            <?php $counter++;  ?>
               
			<div class="carousel-item carousel-item-mobile {{ $counter == 1 ? ' active' : '' }}">				  			  
			    
			    <div class="carousel-caption-mobile">
					<div class="panelNav">
						<div class="container marketing">						
							<div class="row featurette">
								<div id="introWrapper">
									<div id="intro">		   
									   <div class="introPanelTwo">
										  <div class="introPanelTwo-txt">
											 <h1>Our Company</h1>
											 @if ( $image->title != "") <h2>{{ $image->title }}</h2> @endif
											 @if ( $image->description != "") <p>{{ $image->description }}</p> @endif
											 @if ( $image->url != "")<p><a class="btn btn-lg btn-primary" href="{{ $image->url }}" role="button">Click for more</a></p> @endif
										  </div>
									   </div>

									   <div class="introPanelOne"><img alt="" src="{{ url('') }}/images/site/pic1.jpg" title="What we do" alt="What we do" /></div>
									</div>
							   </div>
							</div>
						</div>						
					</div>
				</div>

			</div>
        @endforeach
   
  </div>
</div>

@section('inline-scripts')  
   <script type="text/javascript">        
            $('#myCarousel-mobile').carousel({
		  interval: 15000 // 15 seconds
		})	
    </script>			
@endsection