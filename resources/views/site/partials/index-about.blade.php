<div class="panelHomeAbout-responsive">
    <div class='parallax2-txt-txt-responsive'>
		<h2>About Us</h2>
		<hr>
		{!! $page_about->short_description !!}

		<div class="btn-readmore">
		   <a class="btn-submit" href="{{ url("") }}/pages/{{ $page_events->slug }}">READ MORE</a>
		</div>
	</div>
</div>

<div class="panelHomeAbout">
	<!-- Parallex 2 -->				
	<div class='parallax2-block'>

	  <div class='parallax2-txt'>  	
	    <div class='parallax2-txt-txt'>
			<h2>About Us</h2>
			<hr>
			{!! $page_about->short_description !!}

			<div class="btn-readmore">
			   <a class="btn-submit" href="{{ url("") }}/pages/{{ $page_events->category->slug }}/{{ $page_events->slug }}">READ MORE</a>
			</div>
	    </div>
	  </div>	

	</div>

	<div id="parallax2" class="parallaxParent3 parallax-container3">   
		<div class="parallax2-img">
			<div class="parallax2-img-img"></div>
		</div>
	</div>										
</div>							
							
@section('scripts-parallex')
<script type="text/javascript" src="{{ asset('/js/site/parallex/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/animation.gsap.js') }}"></script>
@endsection
										
@section('inline-scripts-parallex') 
	<script>
		// init controller
		var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		// build scenes				
		new ScrollMagic.Scene({triggerElement: "#parallax2"})
						.setTween("#parallax2 > div", {y: "80%", ease: Linear.easeNone})										
						.addTo(controller);
        //$(".parallax3-img").addClass('parallax3-img-mod');
		
	</script>
@endsection