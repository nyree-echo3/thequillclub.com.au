<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>{{ $category[0]->name }}</h4>
        <ol class="navsidebar list-unstyled">
            @foreach ($side_nav as $item)

				<li class='{{ ($item->slug == $page->slug ? "active" : "") }}'>
					<a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item->slug }}">{{ $item->title }}</a>
				</li>                
                    
				@if (sizeof($item->nav_sub)> 0)
				<ol class="navsidebar navsidebar-sub list-unstyled">
					@foreach ($item->nav_sub as $item_sub)
						<li class='{{ ($item_sub->slug == $page->slug ? "active" : "") }}'>
							<a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item_sub->slug }}">{{ $item_sub->title }}</a>
						</li>
					@endforeach
				</ol>
				@endif								

			@endforeach
       
            @if (strtolower($category[0]->slug) == "events")
				<li class='{{ ($page->slug == "gallery" ? "active" : "") }}'>
					<a class="navsidebar" href="{{ url('') }}/gallery">Gallery</a>
				</li>					
			@endif
        </ol>
    </div>
</div>