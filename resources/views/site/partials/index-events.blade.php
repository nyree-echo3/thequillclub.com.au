<div class="panelNav panelHomeEvents">
	<div class='container'>	               
		  <div class="row">         
			 <div class="col-lg-3 home-col-facebook">
				<div class="home-facebook">
				   @include('site/partials/index-facebook')
				</div>
			 </div>	

			 <div class="col-lg-9 home-events-border home-col-event">
				<div class="home-events">
					@if ($page_events != null)
					<h2>Events</h2>

					{!! $page_events->short_description !!}

					<div class="btn-readmore">				
					   <a class="btn-submit" href="{{ url("") }}/pages/events/{{ $page_events->slug }}">READ MORE</a>
					</div>
					@endif
				</div>
			 </div>						               	    
		  </div>
	  </div>
</div>