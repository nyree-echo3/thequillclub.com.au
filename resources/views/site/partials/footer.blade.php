<footer class='footer'>
 <div class='footerContactsWrapper'>
	 <div class='footerContacts'>
        <div class="panelNav">
	    <div class="container-fluid">
		<div class="row">
       
           <div class="col-lg-2 col-full">
		      <div class="center-block">
			     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-white.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
			  </div>
		   </div>  
      
           <div class="col-lg-2 col-full">
		      <div class="center-block">
			      @if(count($modules))					  
					   @foreach($modules as $nav_item)    
							<div class="footer-txt footer-menu"><a href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}">{{ $nav_item["display_name"] }}</a></div>
					   @endforeach
				   @endif
			   </div>
		   </div>  
       
	       <div class="col-lg-6 col-full">
			  <div class="footer-newsletter">
				  <h2>Newsletter</h2>
				  <h3>Sign up to stay up-to-date with the latest news and events information.</h3>
				  <form id="contact-form" method="post" action="{{ url('contact/save-newsletter') }}">
				      <input type="hidden" name="_token" value="{{ csrf_token() }}">
				      
					  <div class="footer-newsletter-controls">
					  <div class="footer-newsletter-input">
					     <label>Name</label>
					     <input type="textbox" id="name" name="name" required>
					  </div>
					  
					  <div class="footer-newsletter-input">
					     <label>Email</label>
					     <input type="textbox" id="email" name="email" type="email" required>
					  </div>
					  
					  <div class="footer-newsletter-btn">
					     <input type="submit" class="btn-submit" value="GO">
					  </div>
				  </div>
				  </form>
			  </div>
		   </div>  		   		   				   		   
		   
		   <div class="col-lg-2 col-full">			  
			  <div id="footer-social">	
			    <h2>Follow Us</h2>						
				@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif 
				@if ( $social_linkedin != "") <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif            				
			  </div>
		   </div>  
		   		   		   
		</div>
	 </div> 
 </div>
 </div>
  </div>
   
 <div class='footerContainer'>  
  <div id="footer-txt"> 
	@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }} </a> | @endif 
    <a href="{{ url('') }}/contact">Contact</a> |     
    <a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 
    
  </div>
  </div>
</footer>