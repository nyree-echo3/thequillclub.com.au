@if(isset($home_projects))
     <div class="panelNav">
		 <div class=" container home-projects ">
		   <div class="home-projects-row">
			  <h2>Featured projects</h2>
			  <div class="row">  		  
				 @php
					$colCounter = 0;					
				 @endphp

				 @foreach($home_projects as $item) 

					  <div class="col-lg-6 {{ ($colCounter == 1 ? 'col-border' : '') }}">
						   <a href='{{ url('') }}/projects/{{ $item->category->slug }}/{{ $item->slug }}'>				
						       <div class="home-projects-a home-project-a-{{ $colCounter }}">
								 <div class="div-img">
									<img src="{{ url('') }}/{{$item->images[0]->location}}" alt="{{ $item->title }}">
								 </div>
								 <div class="home-projects-txt">{{ $item->title }}</div>   
							   </div>							
						   </a>     				   					 					   					   					  
					  </div>

					  @php 
					  $colCounter++;
					  @endphp
				 @endforeach 	
				</div>
		   </div>
		</div>
    </div>
@endif