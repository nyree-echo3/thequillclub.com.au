<div class="panelNav">
	<div id="myCarousel" class="carousel slide carousel-inner" data-ride="carousel">
	  <div class="carousel-inner inside-page">           
				<div class="carousel-item active">

				  @php
					$imgUrl = "images/site/header.jpg";
					$imgAlt = "";

				    // Module
					if (isset($module))  {
					   $imgUrl = $module->header_image;
					   $imgAlt = $module->display_name;
					} elseif (isset($category) && sizeof($category) > 0)  {
				       // Page Module Category
					   $imgUrl = $category[0]->header;
					   $imgAlt = $category[0]->name;
					}	
					  
				  @endphp

				  <img class="slide" src="{{ url('') }}/{{ $imgUrl }}" alt="{{ $imgAlt }}">				  
				</div>

	  </div>
	</div>
</div>