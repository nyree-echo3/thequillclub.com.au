@if(isset($home_testimonials))
  <div class="panelNav">
	  <div class="home-testimonialsWrapper">
		 <div class="home-testimonials">
		   <div class="container">
			  <h2>Awards</h2>		  
				 <div class="row-testimonial">

					<div class="cycle-testimonials" 
						data-cycle-fx=flipVert
						data-cycle-timeout=20000
						data-cycle-random=true
						data-cycle-slides=span	    
						data-cycle-pause-on-hover="true"
						data-cycle-auto-height=container
						>    


						@foreach($home_testimonials as $item) 		
							<span class='testimonials-slider'>

							<a href='testimonials'>
							<div class='testimonial-slider-txt'>{!! $item->description !!}</div>		
							<!-- <div class='testimonial-slider-by'>company</div> -->				
							</a>

							</span>			
						@endforeach 			
					</div>						
				 </div>	
		   </div>	   	   
		</div>

		<div class="home-testimonials-background">
		</div>
	  </div>
  </div>	
@endif


@section('scripts')
	<script src="{{ asset('/components/jquery-cycle2/build/jquery.cycle2.min.js') }}"></script>
	<script src="{{ asset('/components/jquery-cycle2/src/jquery.cycle2.flip.js') }}"></script>
@endsection

@section('inline-scripts')
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery( '.cycle-testimonials' ).cycle();
	});
	</script>
@endsection