<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Projects</h4>
	<ol class="navsidebar list-unstyled">
	  @foreach ($side_nav as $item)
	     @if ($item->slug != "awards")
		 <li class=''><a class="navsidebar" href="{{ url('') }}/projects/{{ $item->slug }}">{{ $item->name }}</a></li>
		 @endif
	  @endforeach 	                                      
	</ol>		
  </div>          
</div>