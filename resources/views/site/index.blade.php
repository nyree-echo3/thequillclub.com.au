@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')

@include('site/partials/index-about')
@include('site/partials/index-events')

@endsection
