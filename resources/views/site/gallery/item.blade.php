@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-gallery')        
        
        <div class="col-sm-9 blog-main">
          <div class="blog-post">
          
			  <h1>{{ $category->name }}</h1>

			  @if($category->description != "")
				 {!! $category->description !!}
			  @endif

			  @if(isset($items)) 
				 <section class="gallery-block cards-gallery">
					<div class="container">	        
					   <div class="row">

							 @foreach ($items as $item)	

								 <div class="col-lg-2 col-md-4 col-sm-6">
									<div class="card border-0 transform-on-hover">
										<a class="lightbox" href="{{ url('') }}{{$item->location}}" data-caption="{{$item->name}}<br>{!! $item->description !!}">
											<img src="{{ url('') }}{{$item->location}}" alt="{{$item->name}}" class="card-img-top">
										</a>
										<div class="card-body">
											<h6><a href="#">{{$item->name}}</a></h6>
											<p class="text-muted card-text">{!! $item->description !!}</p>
										</div>
									</div>
								</div>							   

							 @endforeach

					   </div>
					</div>
				 </section>  
			  @endif			                                                                                                                                                                                                                                                                                                                                                                  
			</div><!-- /.blog-post -->                                                                                                                                                                                            
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
  </div><!-- /.blog-masthead -->
  
@endsection

@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection

@section('inline-scripts-gallery')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.cards-gallery', { animation: 'slideIn'});
        });
    </script>			
@endsection