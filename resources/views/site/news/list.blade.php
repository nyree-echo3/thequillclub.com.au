<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News"); 
   $meta_keywords_inner = "News"; 
   $meta_description_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        
        <div class="col-sm-12 blog-main">

          <div class="blog-post">     
            <h1 class="blog-post-title">{{ $category_name }}</h1>
            
           <div class="blog-post row">           
                               	        
            @if(isset($items))  
                 @foreach($items as $item)                
                    <div class="col-sm-3">
                       <a href="{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}">
                       <div class="panel-news-item">	
                            <div class="div-img">
                            <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
                            </div>				                                    
                            
                            <div class="panel-news-item-category-date">
                                <span class="panel-news-item-category">{{ $item->category->name }}</span>
								<span class="panel-news-item-date">{{date("M Y", strtotime($item->start_date))}}</span>
                            </div>
                            <div class="panel-news-item-title">{{$item->title}}</div>
                            <div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>
                            
                            <div><a href='{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}'>Read More ></a></div>		
                      </div>                                                 
					  </a>
                    </div>                    
                               
				 @endforeach

                                                                                                                                            
                       <!--                                                                                                                      
                  @foreach($items as $news_item)                 
					  <div class='news-list-item'>							    						    
					    <div class="news-list-item-txt">	  
							<h2 class="blog-post-title">{{$news_item->title}}</h2>
							{!! $news_item["short_description"] !!}
							<a class='btn btn-lg btn-more' href='{{ url('') }}/news/{{ $news_item->category->slug }}/{{$news_item->slug}}'>more</a>
						</div>
						
						<div class="news-list-item-img">
				             @if($news_item->thumbnail != "")
					            <img src="{{ url('') }}/{{ $news_item->thumbnail }}" alt="{{$news_item->title}}" width="140" height="140" />	
					         @endif
					    </div>	
					  </div>                                                          
                   @endforeach
                   -->
                   <!-- Pagination -->
                   <div id="pagination">{{ $items->links() }}</div>
              
               @else
                 <p>Currently there is no news items to display.</p>    
               @endif
          
             </div><!-- /.blog-post -->
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@endsection
